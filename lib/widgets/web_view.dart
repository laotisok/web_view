import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class WebViewTemplate extends StatefulWidget {
  @override
  _WebViewTemplateState createState() => _WebViewTemplateState();
}

class _WebViewTemplateState extends State<WebViewTemplate> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    //configLoading();
    EasyLoading.show(status: 'loading...');
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  List<String> items = ["1", "2", "3", "4", "5", "6", "7", "8"];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    items.add((items.length + 1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
      //   actions: <Widget>[NavigationControls(_controller.future),],
      // ),
      // We're using a Builder here so we have a context that is below the Scaffold
      // to allow calling Scaffold.of(context) so we can show a snackbar.

      // body: Builder(builder: (BuildContext context) {
      //   return Container(
      //     margin: const EdgeInsets.only(
      //       left: 10,
      //       right: 3,
      //       top: 10,
      //       bottom: 10,
      //     ),
      //     child: WebView(
      //       initialUrl: 'https://oboard-dev.appplay.co.kr/',
      //       javascriptMode: JavascriptMode.unrestricted,
      //       onWebViewCreated: (WebViewController webViewController) {
      //         _controller.complete(webViewController);
      //       },
      //       onPageFinished: (String url) {
      //         // should be called when page finishes loading
      //         setState(() {
      //           EasyLoading.dismiss();
      //         });
      //       },
      //       gestureNavigationEnabled: true,
      //     ),
      //   );
      // }),

      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropHeader(),
        // footer: CustomFooter(
        //   builder: (BuildContext context, LoadStatus mode) {
        //     Widget body;
        //     if (mode == LoadStatus.idle) {
        //       body = Text("pull up load");
        //     } else if (mode == LoadStatus.loading) {
        //       body = Text("CupertinoActivityIndicator()");
        //     } else if (mode == LoadStatus.failed) {
        //       body = Text("Load Failed!Click retry!");
        //     } else if (mode == LoadStatus.canLoading) {
        //       body = Text("release to load more");
        //     } else {
        //       body = Text("No more Data");
        //     }
        //     return Container(
        //       height: 55.0,
        //       child: Center(child: body),
        //     );
        //   },
        // ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        // child: ListView.builder(
        //   itemBuilder: (c, i) => Card(child: Center(child: Text(items[i]))),
        //   itemExtent: 100.0,
        //   itemCount: items.length,
        // ),
        child: Container(
          margin: const EdgeInsets.only(
            left: 10,
            right: 3,
            top: 10,
            bottom: 10,
          ),
          child: WebView(
            initialUrl: 'https://oboard-dev.appplay.co.kr/',
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            onPageFinished: (String url) {
              // should be called when page finishes loading
              setState(() {
                EasyLoading.dismiss();
              });
            },
            gestureNavigationEnabled: true,
          ),
        ),
      ),
    );
  }
}
